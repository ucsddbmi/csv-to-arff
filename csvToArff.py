import argparse  # For reading command line arguments
# For getting the filename without the extension
from os.path import basename, splitext

parser = argparse.ArgumentParser(
    description='Convert csv files into arff format.')
parser.add_argument('headerFile', metavar='header', help='the header file')
parser.add_argument('originalFile', metavar='original',
                    help='the original file')
parser.add_argument('subsetFile', metavar='subset', help='the subset file')
parser.add_argument('-H', '--csv-has-headers', dest='csvHasHeaders',
                    action="store_true",
                    help='Boolean: first line of csv file[s] is header')
parser.add_argument('-d', '--delimiter', dest='delimiter',
                    help='String: The delimiter between values. Default: ,',
                    default=',')
parser.add_argument('-D', '--header-delimiter', dest='headerDelimiter',
                    help='String: The delimiter between values in the ' +
                         'header file. Default: ,',
                    default=',')
parser.add_argument('-n', '--nominal-cutoff', dest='nominalCutoff',
                    help='Int: The number of unique values before a nominal ' +
                         'field is treated as a string. -1 means no cutoff ' +
                         'Default: no cutoff',
                    default='-1')
args = parser.parse_args()

nominalCutoff = int(args.nominalCutoff)
# TODO Add a percentage based cutoff

csvHasHeaders = args.csvHasHeaders
delimiter = args.delimiter

originalFile = args.originalFile
originalBasename = splitext(basename(originalFile))[0]
subsetFile = args.subsetFile
subsetBasename = splitext(basename(subsetFile))[0]

headersFileReader = open(args.headerFile, 'r')
headersArray = headersFileReader.readlines()
headersFileReader.close()


# Create Dictionary of headers. Have each value be array/set of unique values
#  for each column

uniqueValues = {}


columnOrder = []
attributeStr = ""


# Split headerFile lines into an array ['name', 'data_type']
for (i, column) in enumerate(headersArray):
    headersArray[i] = [x.strip() for x in column.split(args.headerDelimiter)]
    # Initialize empty sets inuniqueValues if the column is a string
    columnName = headersArray[i][0]
    columnType = headersArray[i][1]
    if columnType == 'string':
        uniqueValues[columnName] = set()
    pass
pass


# Itterate through the orignial file, and tally all options of strings
with open(originalFile, 'r') as csvFileReader:
    # If the data files have a header row, strip it, and create an
    # array of the headers. Then rearrange the headers from the
    # headers file
    if csvHasHeaders:
        # Pull headers from csv
        line = csvFileReader.readline()
        csvHeaders = [x.strip() for x in line.split(delimiter)]
        # Make a copy of the headers array
        tmpHeaderArray = headersArray[:]
        # Loop through the headers from the csv to reorder the headers array
        for (i, header) in enumerate(csvHeaders):
            for (j, row) in enumerate(tmpHeaderArray):
                if (row[0] == header):
                    # Overwrite the headers array
                    headersArray[i] = row
                    del tmpHeaderArray[j]
                    break
                else:
                    continue
            pass
        pass
    pass
    for line in csvFileReader:
        for (i, value) in enumerate(line.split(delimiter)):
            value = value.strip()
            columnName = headersArray[i][0]
            columnType = headersArray[i][1]
            if columnType == 'string' and value != '':
                uniqueValues[columnName].add(value)
            pass
        pass
    pass
pass

# TODO replace set with string as array in headers section

for row in headersArray:
    columnName = row[0]
    columnType = row[1]
    if (columnType == 'string'):
        if(nominalCutoff == -1 or
           len(uniqueValues[columnName]) < nominalCutoff):
            columnType = str(uniqueValues[columnName])
        pass
    pass
    attributeStr += "@ATTRIBUTE " + \
        columnName + "\t" + columnType + "\n"
pass


originalArff = originalBasename + '.arff'
arffWriter = open(originalArff, 'w')

# Print header and relations to originalFile.arff
arffWriter.write("@RELATION \"" + originalBasename + "\" \n\n")
arffWriter.write(attributeStr + "\n\n")
arffWriter.write("@DATA\n")
# Print the originalFile data in line by line to avoid mem overflow
with open(originalFile, 'r') as csvFileReader:
    # Skip first line
    if csvHasHeaders:
        line = csvFileReader.readline()
    pass
    for line in csvFileReader:
        arffWriter.write(line)
    pass
pass
arffWriter.close()


subsetArff = subsetBasename + '.arff'
arffWriter = open(subsetArff, 'w')

# Print header and relations to originalFile.arff
arffWriter.write("@RELATION \"" + subsetBasename + "\" \n\n")
arffWriter.write(attributeStr + "\n\n")
arffWriter.write("@DATA\n")
# Print the originalFile data in line by line to avoid mem overflow
with open(subsetFile, 'r') as csvFileReader:
    # Skip first line
    if csvHasHeaders:
        line = csvFileReader.readline()
    pass
    for line in csvFileReader:
        arffWriter.write(line)
    pass
pass
arffWriter.close()

print("Done")
