usage: csvToArff.py [-h] [-H] [-d DELIMITER] [-D HEADERDELIMITER]
                    [-n NOMINALCUTOFF]
                    header original subset

Convert csv files into arff format.

positional arguments:

  header                the header file
  
  original              the original file
  
  subset                the subset file
  

optional arguments:

  -h, --help            show this help message and exit
  
  -H, --csv-has-headers
                        Boolean: first line of csv file[s] is header
                        
  -d DELIMITER, --delimiter DELIMITER
                        String: The delimiter between values. Default: ,
                        
  -D HEADERDELIMITER, --header-delimiter HEADERDELIMITER
                        String: The delimiter between values in the header
                        file. Default: ,
                        
  -n NOMINALCUTOFF, --nominal-cutoff NOMINALCUTOFF
                        Int: The number of unique values before a nominal
                        field is treated as a string. -1 means no cutoff
                        Default: no cutoff
